var gulp = require('gulp');
var gulpSass = require('gulp-sass');

// budowanie plikow .css na podstawie .scss
gulp.task('buildcss', function() {
    return gulp.src('./src/main/webapp/dev-assets/style.scss')
        .pipe(gulpSass())
        .pipe(gulp.dest('./src/main/webapp/prod-assets'));
});

// sledzenie zmian w plikach .scss
// jesli sa to uruchom zadanie
gulp.task('watch', function() {
   gulp.watch('./src/main/webapp/dev-assets/**/*.scss', gulp.series('buildcss'));
});