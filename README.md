Dev tools:
1. Create package.json -> npm init
2. Install gulp (globally)-> npm i gulp-cli -g
3. Install gulp (on project) -> npm i gulp --save-dev
4. Install gulp-sass -> npm i gulp-sass --save-dev

To build .scss -> .css:
'gulpfile.js' contains tasks 'buildcss' and 'watch'.
'buildcss' -> command to build .scss to .css
'watch' -> analyze changes in .scss files and run 'buildcss' if they appear